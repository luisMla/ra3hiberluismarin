package giu;

import base.Cliente;
import tareas.TareaCliente;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import static util.Constantes.*;

public class PanelCliente extends Observable implements ActionListener ,ListSelectionListener, PropertyChangeListener {
    public JPanel panel1;
    private JTextField tfNombreCliente;
    private JLabel lblNombre;
    private JTextField tfApellido;
    private JButton btAlta;
    private JList listCli;
    private JButton btBorrar;
    private JButton btModificar;
    private JPanel panelRepintar;
    private JButton btDispose;
    public Cliente cliente;
    public DefaultListModel<Cliente> dlmCliente;
    ArrayList<Cliente> clientes;
    TareaCliente tareaCliente;

    Observer observer;

    public PanelCliente(Observer observer) {

        btAlta.setActionCommand(AC_CLIENTE_A);
        btModificar.setActionCommand(AC_CLIENTE_M);
        btBorrar.setActionCommand(AC_CLIENTE_D);
        btDispose.setActionCommand(AC_DISPOSE);
        this.observer = observer;

        dlmCliente = new DefaultListModel<>();
        listCli.setModel(dlmCliente);
        clientes = new ArrayList<>();
        tareaCliente = new TareaCliente(clientes,dlmCliente,"");
        initActionListener(this);
        initListSelectionListener(this);
        initPropertyChangeListener(this);
        tareaCliente.execute();
    }

    private void initActionListener(ActionListener actionListener) {
        btAlta.addActionListener(actionListener);
        btModificar.addActionListener(actionListener);
        btBorrar.addActionListener(actionListener);
        btDispose.addActionListener(actionListener);
    }

    private void initListSelectionListener(ListSelectionListener listSelectionListener) {
        listCli.addListSelectionListener(listSelectionListener);
    }
    private void initPropertyChangeListener(PropertyChangeListener propertyChangeListener){
        tareaCliente.addPropertyChangeListener(propertyChangeListener);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        tareaCliente.cancel(false);
        switch (e.getActionCommand()) {
            case AC_CLIENTE_A:
                cliente = new Cliente();
                montarCliente(tfNombreCliente.getText(), tfApellido.getText());
                observer.update(this, AC_CLIENTE_A);
                tareaCliente = new TareaCliente(cliente,clientes,dlmCliente,ALTA);
                initPropertyChangeListener(this);
                tareaCliente.execute();

                break;
            case AC_CLIENTE_M:
                montarCliente(tfNombreCliente.getText(), tfApellido.getText());
                observer.update(this, AC_CLIENTE_M);
                tareaCliente = new TareaCliente(cliente,clientes,dlmCliente,MODI);
                initPropertyChangeListener(this);
                tareaCliente.execute();
                break;
            case AC_CLIENTE_D:
                observer.update(this, AC_CLIENTE_D);
                tareaCliente = new TareaCliente(cliente,clientes,dlmCliente,BAJA);
                initPropertyChangeListener(this);
                tareaCliente.execute();
                break;
            case AC_DISPOSE:
                dispose();
                observer.update(this,DISPOSE_PANEL_CLIENTE);
                tareaCliente.cancel(false);
                break;

        }
        System.out.println("fuera swich action comand");

    }

    @Override
    public synchronized void addObserver(Observer o) {
        this.observer = o;
    }

    @Override
    public void notifyObservers() {

    }

    private void montarCliente(String nombre, String apellido) {
        cliente.setNombre(nombre);
        cliente.setApellido(apellido);
    }


    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()==true)
        if (e.getSource().equals(listCli)) {
            cliente = (Cliente) listCli.getSelectedValue();
            tfNombreCliente.setText(cliente.getNombre());
            tfApellido.setText(cliente.getApellido());
        }
    }



    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equals(ALTA)) {
            System.out.println("alta");
        }
        if (evt.getPropertyName().equals(BAJA)) {
            System.out.println("baja");
        }
        if (evt.getPropertyName().equals(MODI)) {
            System.out.println("modi");
        }
        if (evt.getPropertyName().equals("done")){
            System.out.println("done");
        }
        if(evt.getPropertyName().equals("dlm")){
            System.out.println("dlm");
            dlmCliente = (DefaultListModel<Cliente>) evt.getNewValue();
            listCli.setModel(dlmCliente);
            observer.update(this,"dlm");
        }
        if(evt.getPropertyName().equals("cliente")){
            System.out.println("cliente");
        }
    }

    private void refrescar(){
       // TareaCliente tareaCliente = new TareaCliente(null, clientes,"");

        //while (tareaCliente.getState().toString().equals(equals(PENDING))){
           // System.out.println(PENDING+" "+ tareaCliente.getState().toString());
        //}
       // System.out.println(DONE+" "+ tareaCliente.getState().toString());
        dlmCliente = new DefaultListModel<>();
        for (Cliente cli : clientes) {
            dlmCliente.addElement(cli);
        }

    }
    private void dispose(){
        dlmCliente = new DefaultListModel<>();
        cliente = new Cliente();
        clientes = new ArrayList<>();
        panel1.removeAll();
    }


}
