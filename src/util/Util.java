package util;

import javax.swing.*;
import java.text.DecimalFormat;
import java.text.ParseException;

public class Util {
    public static void mensajeError(String mensaje, String titulo) {
        JOptionPane.showMessageDialog(null, mensaje, titulo, JOptionPane.ERROR_MESSAGE);
    }

    /**
     * lanza un mensaje de informacion con un JOptionPane
     * @param mensaje {@link String}
     * @param titulo {@link String}
     */
    public static void mensajeInformacion(String mensaje, String titulo) {
        JOptionPane.showMessageDialog(null, mensaje, titulo, JOptionPane.INFORMATION_MESSAGE);
    }

}
