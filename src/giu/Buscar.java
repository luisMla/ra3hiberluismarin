package giu;

import base.*;
import tareas.*;
import util.Constantes;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Observable;
import java.util.Observer;

import static util.Constantes.AC_DISPOSE;

public class Buscar extends Observable implements ActionListener,ListSelectionListener,
        PropertyChangeListener , KeyListener{

    private JPanel panel1;
    private JPanel panelNorte;
    private JPanel panelSur;
    private JPanel panelEste;
    private JPanel panelOpeste;
    private JPanel panelCentro;
    private JTextField tfBuscar;
    private JButton btLimpiar;
    private JList list1;
    private JLabel lblTipoSeleccionado;
    private JButton btGoDispose;

    ArrayList<Object>objects;
    DefaultListModel<Object>dlmObject;

    TareaCarrito tareaCarrito;
    TareaCarritoProducto tareaCarritoProducto;
    TareaCliente tareaCliente;
    TareaOperario tareaOperario;
    TareaProducto tareaProducto;
    TareaDir tareaDir;

    private Observer observer;

    public Buscar(Observer observer) {
        this.observer = observer;
        objects = new ArrayList<>();
        dlmObject = new DefaultListModel<>();
        btGoDispose.setActionCommand(AC_DISPOSE);
//todo hacabar panel
    }
    private void initActionListener(ActionListener actionListener){
        btGoDispose.addActionListener(actionListener);
        btLimpiar.addActionListener(actionListener);
    }
    private void initKeListener(KeyListener keyListener){
        tfBuscar.addKeyListener(keyListener);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if(evt.getPropertyName().equals("operarios")){
            //operarios
            objects.addAll((Collection<?>) evt.getNewValue());
            tareaOperario.cancel(false);
            tareaProducto.execute();
        }
        if(evt.getPropertyName().equals("productos")){
            //Productos
            objects.addAll((Collection<?>) evt.getNewValue());
            tareaProducto.cancel(false);
            tareaCliente.execute();
        }
        if(evt.getPropertyName().equals("cliente")){
            //cliente
            objects.addAll((Collection<?>) evt.getNewValue());
            tareaCliente.cancel(false);
            tareaDir.execute();
        }
        if(evt.getPropertyName().equals("Ditecciones")){
            //direcciones
            objects.addAll((Collection<?>) evt.getNewValue());
            tareaDir.cancel(false);
            tareaCarrito.execute();
        }
        if(evt.getPropertyName().equals("carritos")){
            //carritos
            objects.addAll((Collection<?>) evt.getNewValue());
            tareaCarrito.cancel(false);
            tareaCarritoProducto.execute();
        }
        if(evt.getPropertyName().equals("carrot")){
            //CarritoProducto
            objects.addAll((Collection<?>) evt.getNewValue());
            tareaCarritoProducto.cancel(false);
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
    private void lanzarTareas(){
        tareaOperario = new TareaOperario(null,null,"");
        tareaProducto = new TareaProducto (new ArrayList<Producto>(),new DefaultListModel<Producto>(),"");
        tareaCliente = new TareaCliente(new ArrayList<Cliente>(),new DefaultListModel<Cliente>(),"");
        tareaDir = new TareaDir(new ArrayList<DireccionesEnvio>(),new DefaultListModel<DireccionesEnvio>(),"");
        tareaCarrito = new TareaCarrito(new ArrayList<Carrito>(),new DefaultListModel<Carrito>(),"");
        tareaCarritoProducto =  new TareaCarritoProducto(new ArrayList<CarritoProducto>(),new DefaultListModel<CarritoProducto>(),"");
        tareaOperario.execute();
    }
}
