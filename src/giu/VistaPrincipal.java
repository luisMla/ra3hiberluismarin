package giu;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import static util.Constantes.*;

public class VistaPrincipal {
    public JFrame frame;
    public JPanel panel1;
    public JLabel lblPanelCargado;
    public JLabel lblInfo;
    public JScrollPane jcpScr;
    public JPanel panelCentro;
    public JMenuBar jMenuBar;
    //jmenus
    public JMenu jMenuArchivo;
    public JMenu jMenuCarr;
    public JMenu jMenuCarrProd;
    public JMenu jMenuCliente;
    public JMenu jMenuDirEnv;
    public JMenu jMenuOperario;
    public JMenu jMenuProd;
    //jmenuItems
    public JMenuItem miAltaCarr;
    public JMenuItem miBajaCarr;
    public JMenuItem miModCarr;

    public JMenuItem miAltaCarrProd;
    public JMenuItem miBajaCarrProd;
    public JMenuItem miModCarrProd;

    public JMenuItem miAltaCliente;
    public JMenuItem miBajaCliente;
    public JMenuItem miModCliente;

    public JMenuItem miAltaDirEnv;
    public JMenuItem miBajaDirEnv;
    public JMenuItem miModDirEnv;

    public JMenuItem miAltaOperario;
    public JMenuItem miBajaOperario;
    public JMenuItem miModOperario;

    public JMenuItem miAltaProd;
    public JMenuItem miBajaProd;
    public JMenuItem miModProd;

    public JMenuItem miSalir;






    public VistaPrincipal() {
        //SplashScreen s = new SplashScreen();
        frame = new JFrame("VistaPrincipal");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        panelCentro.setLayout(new BoxLayout(panelCentro,BoxLayout.Y_AXIS));
        montarMenu();
        frame.setJMenuBar(jMenuBar);


        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        //s.start();
    }

    public void montarMenu(){
        jMenuBar = new JMenuBar();

        //inicializar menus
        jMenuArchivo = new JMenu(ARCHIVO);
        jMenuCarr = new JMenu(CARRITO);
        jMenuCarrProd = new JMenu(CARRITO_PRODUCTO);
        jMenuCliente = new JMenu(CLIENTE);
        jMenuDirEnv = new JMenu(DRIRECCIONES_ENVIO);
        jMenuOperario = new JMenu(OPERARIO);
        jMenuProd = new JMenu(PRODUCTO);

        //montar menu Archivo
        miSalir = new JMenuItem(SALIR);
        miSalir.setActionCommand(SALIR);
        jMenuArchivo.add(miSalir);

        //montar menu carritos todo
        miAltaCarr = new JMenuItem(OPERACIONES + CARRITO);
        miAltaCarr.setActionCommand(AC_CARRITO_A);
        /*
        miBajaCarr = new JMenuItem(BAJA);
        miBajaCarr.setActionCommand(AC_CARRITO_D);
        miModCarr = new JMenuItem(MODI);
        miModCarr.setActionCommand(AC_CARRITO_M);
        */
        jMenuCarr.add(miAltaCarr);
        //jMenuCarr.add(miBajaCarr);
        //jMenuCarr.add(miModCarr);

        // montar Carrito producto todo
        miAltaCarrProd = new JMenuItem(OPERACIONES+CARRITO_PRODUCTO);
        miAltaCarrProd.setActionCommand(AC_CARRITO_PRODUCTO_A);
        /*
        miBajaCarrProd = new JMenuItem(BAJA);
        miBajaCarrProd.setActionCommand(AC_CARRITO_PRODUCTO_D);
        miModCarrProd = new JMenuItem(MODI);
        miModCarrProd.setActionCommand(AC_CARRITO_PRODUCTO_M);
        */
        jMenuCarrProd.add(miAltaCarrProd);
        //jMenuCarrProd.add(miBajaCarrProd);
        //jMenuCarrProd.add(miModCarrProd);

        //montar Cliente todo
        miAltaCliente = new JMenuItem(OPERACIONES + CLIENTE);
        miAltaCliente.setActionCommand(AC_CLIENTE_A);
        /*
        miBajaCliente = new JMenuItem(BAJA);
        miBajaCliente.setActionCommand(AC_CLIENTE_D);
        miModCliente = new JMenuItem(MODI);
        miModCliente.setActionCommand(AC_CLIENTE_D);
        */

        jMenuCliente.add(miAltaCliente);
        //jMenuCliente.add(miBajaCliente);
        //jMenuCliente.add(miModCliente);

        //montar Direcciones Envio todo
        miAltaDirEnv = new JMenuItem(OPERACIONES + DRIRECCIONES_ENVIO);
        miAltaDirEnv.setActionCommand(AC_DRIRECCIONES_ENVIO_A);
        /*
        miBajaDirEnv = new JMenuItem(BAJA);
        miBajaDirEnv.setActionCommand(AC_DRIRECCIONES_ENVIO_D);
        miModDirEnv = new JMenuItem(MODI);
        miModDirEnv.setActionCommand(AC_DRIRECCIONES_ENVIO_M);
        */
        jMenuDirEnv.add(miAltaDirEnv);
        //jMenuDirEnv.add(miBajaDirEnv);
        //jMenuDirEnv.add(miModDirEnv);

        //montar Operarios todo
        miAltaOperario = new JMenuItem(OPERACIONES + OPERARIO);
        miAltaOperario.setActionCommand(AC_OPERARIO_A);
        /*
        miBajaOperario = new JMenuItem(BAJA);
        miBajaOperario.setActionCommand(AC_OPERARIO_D);
        miModOperario = new JMenuItem(MODI);
        miModOperario.setActionCommand(AC_OPERARIO_M);
        */

        jMenuOperario.add(miAltaOperario);
        //jMenuOperario.add(miBajaOperario);
        //jMenuOperario.add(miModOperario);

        //motar Productos todo
        miAltaProd = new JMenuItem(OPERACIONES + PRODUCTO);
        miAltaProd.setActionCommand(AC_PRODUCTO_A);
        /*
        miBajaProd = new JMenuItem(BAJA);
        miBajaProd.setActionCommand(AC_PRODUCTO_D);
        miModProd = new JMenuItem(MODI);
        miModProd.setActionCommand(AC_PRODUCTO_M);
        */
        jMenuProd.add(miAltaProd);
        //jMenuProd.add(miBajaProd);
        //jMenuProd.add(miModProd);

        //add menus a la barra
        jMenuBar.add(jMenuArchivo);
        jMenuBar.add(jMenuOperario);
        jMenuBar.add(jMenuCliente);
        jMenuBar.add(jMenuProd);
        jMenuBar.add(jMenuCarr);
        jMenuBar.add(jMenuCarrProd);
        jMenuBar.add(jMenuDirEnv);


    }

}
