package base;

import javax.persistence.*;

@Entity
@Table(name = "direcciones_envio", schema = "hibernate", catalog = "")
public class DireccionesEnvio {
    private int id;
    private String calle;
    private String codigoPosta;
    private Cliente clienteByIdCliente;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "calle")
    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    @Basic
    @Column(name = "codigo_posta")
    public String getCodigoPosta() {
        return codigoPosta;
    }

    public void setCodigoPosta(String codigoPosta) {
        this.codigoPosta = codigoPosta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DireccionesEnvio that = (DireccionesEnvio) o;

        if (id != that.id) return false;
        if (calle != null ? !calle.equals(that.calle) : that.calle != null) return false;
        if (codigoPosta != null ? !codigoPosta.equals(that.codigoPosta) : that.codigoPosta != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (calle != null ? calle.hashCode() : 0);
        result = 31 * result + (codigoPosta != null ? codigoPosta.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "id_cliente", referencedColumnName = "id")
    public Cliente getClienteByIdCliente() {
        return clienteByIdCliente;
    }

    public void setClienteByIdCliente(Cliente clienteByIdCliente) {
        this.clienteByIdCliente = clienteByIdCliente;
    }

    @Override
    public String toString() {
        return "DireccionesEnvio{" +
                "id=" + id +
                ", calle='" + calle + '\'' +
                ", codigoPosta='" + codigoPosta + '\'' +
                ", clienteByIdCliente=" + clienteByIdCliente +
                '}';
    }
}
