package giu;

import base.*;
import org.hibernate.Session;
import tareas.TareaCarrito;
import tareas.TareaCarritoProducto;
import tareas.TareaProducto;

import javax.persistence.Query;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import static util.Constantes.*;

public class PanelCarritoProducro extends Observable implements ActionListener,ListSelectionListener, PropertyChangeListener {
    private final static String AC_CONSULTA = "AC_CONSULTA";
    public JPanel panel1;
    private JButton btAlta;
    private JList listProd;
    private JButton btBorrar;
    private JButton btModificar;
    private JPanel panelRepintar;
    private JButton btDispose;
    private JList listCarr;
    private JLabel lblCantidad;
    private JTextField tfCantidad;
    private JList listCarProd;
    private JRadioButton rbConsultar;

    private Observer observer;
    private TareaCarrito tareaCarrito;
    private TareaProducto tareaProducto;
    private Carrito carrito;
    private ArrayList<Carrito>carritos;
    private DefaultListModel<Carrito>dlmCarrito;
    private Producto producto;
    private ArrayList<Producto>productos;
    private DefaultListModel<Producto>dlmProducto;
    private CarritoProducto carritoProducto;
    private DefaultListModel<CarritoProducto>dlmCarritoProducto;
    private TareaCarritoProducto tareaCarritoProducto;
    private ArrayList<CarritoProducto> carritoProductoArrayList;

    public PanelCarritoProducro(Observer observer) {
        this.observer = observer;
        btAlta.setActionCommand(AC_CARRITO_PRODUCTO_A);
        btModificar.setActionCommand(AC_CARRITO_PRODUCTO_M);
        btBorrar.setActionCommand(AC_CARRITO_PRODUCTO_D);
        btDispose.setActionCommand(AC_DISPOSE);
        rbConsultar.setActionCommand(AC_CONSULTA);

        //btBorrar.setEnabled(false);
        //btModificar.setEnabled(false);

        carritos = new ArrayList<>();
        dlmCarrito = new DefaultListModel<>();
        productos = new ArrayList<>();
        dlmProducto = new DefaultListModel<>();

        carritoProductoArrayList = new ArrayList<>();
        dlmCarritoProducto = new DefaultListModel<>();


        initActionListener(this);
        initListSelectionListener(this);
        initRefresh();
    }
    private void initActionListener(ActionListener actionListener) {
        btAlta.addActionListener(actionListener);
        btModificar.addActionListener(actionListener);
        btBorrar.addActionListener(actionListener);
        btDispose.addActionListener(actionListener);
        rbConsultar.addActionListener(actionListener);
    }

    private void initListSelectionListener(ListSelectionListener listSelectionListener) {
        listCarr.addListSelectionListener(listSelectionListener);
        listProd.addListSelectionListener(listSelectionListener);
        listCarProd.addListSelectionListener(listSelectionListener);
    }
    private void initPropertyChangeListener(PropertyChangeListener propertyChangeListener){
        tareaCarrito.addPropertyChangeListener(propertyChangeListener);
        tareaProducto.addPropertyChangeListener(propertyChangeListener);
        tareaCarritoProducto.addPropertyChangeListener(propertyChangeListener);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        allStop();
        switch (e.getActionCommand()){
            case AC_CARRITO_PRODUCTO_A:
                carritoProducto = new CarritoProducto();
                carritoProducto.setCarritoByIdCarrito(carrito);
                carritoProducto.setProductoByIdProducto(producto);
                carritoProducto.setCantidad(Integer.parseInt(tfCantidad.getText()));
                crud(carritoProducto,ALTA);
                observer.update(this,AC_CARRITO_PRODUCTO_A);
                break;
            case AC_CARRITO_PRODUCTO_M:
                //carritoProducto.setCarritoByIdCarrito(carrito);
                carritoProducto.setCantidad(Integer.parseInt(tfCantidad.getText()));
                //carritoProducto.setProductoByIdProducto(producto);
                crud(carritoProducto,MODI);
                observer.update(this,AC_CARRITO_PRODUCTO_M);

                break;
            case AC_CARRITO_PRODUCTO_D:
                crud(carritoProducto,BAJA);
                observer.update(this,AC_CARRITO_PRODUCTO_D);
                break;
            case AC_DISPOSE:
                tareaCarrito.cancel(false);
                tareaProducto.cancel(false);
                tareaCarritoProducto.cancel(false);
                tareaCarritoProducto.cancel(false);
                observer.update(this,DISPOSE_PANEL_CARRITO_PRODUCTO);
                break;
            case AC_CONSULTA:
                if(rbConsultar.isSelected()){
                    /*btAlta.setEnabled(false);
                    btModificar.setEnabled(true);
                    btBorrar.setEnabled(true);*/

                }else {
                   /* btAlta.setEnabled(true);
                    btModificar.setEnabled(false);
                    btBorrar.setEnabled(false);
                    initRefresh();*/
                }
                initRefresh();
                break;
        }


    }
    private void crud(CarritoProducto carritoProducto, String condicion){
        tareaCarritoProducto = new TareaCarritoProducto(carritoProducto,carritoProductoArrayList,dlmCarritoProducto,condicion);
        tareaCarritoProducto.addPropertyChangeListener(this);
        tareaCarritoProducto.execute();
        while (!tareaCarritoProducto.isDone()){
            System.out.println("haciendo algo---------------------------------------------");
        }
        initRefresh();
    }
    private void initRefresh(){
        System.out.println("initRefresh");
        tareaCarrito = new TareaCarrito(carritos,dlmCarrito,"");
        tareaProducto = new TareaProducto(productos, dlmProducto,"");
        tareaCarritoProducto = new TareaCarritoProducto(carritoProductoArrayList,dlmCarritoProducto,"");
        initPropertyChangeListener(this);
        tareaCarrito.execute();
    }
    private void allStop(){
        System.out.println("allStop");
        tareaCarrito.cancel(false);
        tareaProducto.cancel(false);
        tareaCarritoProducto.cancel(false);
    }
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if(evt.getPropertyName().equals("dlmCarr")){
            dlmCarrito = (DefaultListModel<Carrito>) evt.getNewValue();
            listCarr.setModel(dlmCarrito);
            observer.update(this,"dlmCarr");
            if(tareaProducto.getState().toString().equals(PENDING)){
                tareaProducto.execute();
                System.out.println( "dlmCarr lanza tarea producto");
            }
            if(rbConsultar.isSelected()){
               // listarCarProd(carrito);
            }
        }
        if(evt.getPropertyName().equals("dlmPr")){
            if(!rbConsultar.isSelected()){
                dlmProducto = (DefaultListModel<Producto>) evt.getNewValue();
                listProd.setModel(dlmProducto);
                if(tareaCarritoProducto.getState().toString().equals(PENDING)){
                    tareaCarritoProducto.execute();//todo revisar
                    System.out.println("dlmPr Lanza tarea carProd");
                }
            }
        }
        if(evt.getPropertyName().equals("dlmCp")){
            dlmCarritoProducto = (DefaultListModel<CarritoProducto>) evt.getNewValue();
            listCarProd.setModel(dlmCarritoProducto);
            if(tareaCarrito.getState().toString().equals(PENDING)){
                tareaCarrito.execute();

            }
        }
        if(evt.getPropertyName().equals("crud")){
            initRefresh();
            System.out.println("init refresh");
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()==true) {
            if (e.getSource().equals(listCarr)) {
                carrito = (Carrito) listCarr.getSelectedValue();
                System.out.println("-----------------------<>Pillo Carrito");

                System.out.println("<carrito>"+carrito+"\n<Producto>"+producto+"\n<CarritoProducto>"+carritoProducto);
                if(rbConsultar.isSelected())
                    listarCarProd(carrito);

            }
            if (e.getSource().equals(listProd)) {
                producto = (Producto) listProd.getSelectedValue();
                System.out.println("-----------------------<>Pillo Producto");
                System.out.println("<carrito>"+carrito+"\n<Producto>"+producto+"\n<CarritoProducto>"+carritoProducto);
            }
            if(e.getSource().equals(listCarProd)){
                carritoProducto = (CarritoProducto) listCarProd.getSelectedValue();
                listCarr.setSelectedValue(carritoProducto.getCarritoByIdCarrito(),true);
                listProd.setSelectedValue(carritoProducto.getProductoByIdProducto(),true);
                carrito = carritoProducto.getCarritoByIdCarrito();
                producto = carritoProducto.getProductoByIdProducto();
                System.out.println("-----------------------<>Pillo CarritoProducto");
                System.out.println("<carrito>"+carrito+"\n<Producto>"+producto+"\n<CarritoProducto>"+carritoProducto);

            }
        }
    }
    private void listarCarProd(Carrito carrito){
        dlmCarritoProducto = new DefaultListModel<>();
        for (CarritoProducto capr : carrito.getCarritoProductosById()) {
            dlmCarritoProducto.addElement(capr);
        }
        listCarProd.setModel(dlmCarritoProducto);
    }
}
