package contoladores;

import base.HibernateUtil;
import giu.*;
import logica.Modelo;
import util.Util;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Observable;
import java.util.Observer;
import java.util.Properties;

import static util.Constantes.*;

public class Controller implements ActionListener, WindowListener,Observer{//todo comunicar paneles
    VistaPrincipal vista;
    Modelo modelo ;
    private File ruta = new File("fichero.conf");
    private String usuario;
    private String password;

    public Controller(VistaPrincipal vista,Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;
        initWinListener(this);
        initActionListener(this);
    }
    private void initActionListener(ActionListener actionListener){
        vista.miAltaCliente.addActionListener(actionListener);
        vista.miAltaOperario.addActionListener(actionListener);
        vista.miAltaProd.addActionListener(actionListener);
        vista.miAltaCarr.addActionListener(actionListener);
        vista.miAltaCarrProd.addActionListener(actionListener);
        vista.miAltaDirEnv.addActionListener(actionListener);
        vista.miSalir.addActionListener(actionListener);
    }
    private void initWinListener(WindowListener windowListener){
        vista.frame.addWindowListener(windowListener);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case AC_CLIENTE_A:
                PanelCliente panelCliente = new PanelCliente( this);
                vista.panelCentro.add(panelCliente.panel1);
                vista.panelCentro.revalidate();
                vista.lblPanelCargado.setText("Ultimo Panel Cargado OperacionesCliente");
                break;
            case AC_OPERARIO_A:
                PanelOperario panelOperario = new PanelOperario(this);
                vista.panelCentro.add(panelOperario.panel1);
                vista.panelCentro.revalidate();
                vista.lblPanelCargado.setText("Ultimo Panel Cargado OperacionesOperarios");
                break;
            case AC_PRODUCTO_A:
                PanelProducto panelProducto = new PanelProducto(this);
                vista.panelCentro.add(panelProducto.panel1);
                vista.panelCentro.revalidate();
                vista.lblPanelCargado.setText("Ultimo panel cargado Operaciones Producto");
                break;
            case AC_CARRITO_A:
                PanelCarrito panelCarrito = new PanelCarrito(this);
                vista.panelCentro.add(panelCarrito.panel1);
                vista.lblPanelCargado.setText("Ultimo panel cargado Operaciones Carritos");
                break;
            case AC_CARRITO_PRODUCTO_A:
                PanelCarritoProducro panelCarritoProducro = new PanelCarritoProducro(this);
                vista.panelCentro.add(panelCarritoProducro.panel1);
                vista.lblPanelCargado.setText("Ultimo panel cargado Operaciones Carrito Producto");
                break;
            case AC_DRIRECCIONES_ENVIO_A:
                vista.lblPanelCargado.setText("Ultimo panel cargado Operaciones Direcciones de envio");
                PanelDirecciones panelDirecciones = new PanelDirecciones(this);
                vista.panelCentro.add(panelDirecciones.panel1);
                break;
            case SALIR:
                System.exit(0);
                break;
        }
        vista.panelCentro.revalidate();
        vista.panelCentro.repaint();
    }

    @Override
    public void update(Observable o, Object arg) {
        if(arg.equals(AC_CLIENTE_A))
            vista.lblInfo.setText("Alta Cliente");
        if(arg.equals(AC_CLIENTE_D))
            vista.lblInfo.setText("Borrado Cliente");
        if(arg.equals(AC_CLIENTE_M))
            vista.lblInfo.setText("Modificado Cliente");
        if(arg.equals(DISPOSE_PANEL_CLIENTE)){
            PanelCliente panelCliente = (PanelCliente) o;
            vista.panelCentro.remove(panelCliente.panel1);
            vista.panelCentro.revalidate();
            vista.panelCentro.repaint();
        }
        if(arg.equals("dlm")){
            vista.lblInfo.setText("Ultima Actualizacion Clientes"+ LocalDateTime.now());
        }

        if(arg.equals(AC_OPERARIO_A))
            vista.lblInfo.setText("Alta Operario");
        if(arg.equals(AC_OPERARIO_D))
            vista.lblInfo.setText("Borrado Operario");
        if(arg.equals(AC_OPERARIO_M))
            vista.lblInfo.setText("Modificado Operario");
        if(arg.equals(DISPOSE_PANEL_OPERARIO)){
            PanelOperario panelOperario = (PanelOperario) o;
            vista.panelCentro.remove(panelOperario.panel1);
            vista.panelCentro.revalidate();
            vista.panelCentro.repaint();
        }
        if(arg.equals("dlmO")){
            vista.lblInfo.setText("Ultima Actualizacion Operarios "+ LocalDateTime.now());
        }

        if(arg.equals(AC_PRODUCTO_A))
            vista.lblInfo.setText("Alta Producto");
        if(arg.equals(AC_PRODUCTO_D))
            vista.lblInfo.setText("Borrado Producto");
        if(arg.equals(AC_PRODUCTO_M))
            vista.lblInfo.setText("Modificado Producto");
        if(arg.equals(DISPOSE_PANEL_PRODUCTO)){
            PanelProducto panelProducto = (PanelProducto) o;
            vista.panelCentro.remove(panelProducto.panel1);
            vista.panelCentro.revalidate();
            vista.panelCentro.repaint();
        }
        if(arg.equals("dlmPr")){
            vista.lblInfo.setText("Ultima Actualizacion Productos "+ LocalDateTime.now());
        }
        if(arg.equals(AC_CARRITO_A))
            vista.lblInfo.setText("Alta Carrito");
        if(arg.equals(AC_CARRITO_D))
            vista.lblInfo.setText("Borrado Carrito");
        if(arg.equals(AC_CARRITO_M))
            vista.lblInfo.setText("Modificado Carrito");
        if(arg.equals(DISPOSE_PANEL_CARRITO)){
            PanelCarrito panelCarrito = (PanelCarrito) o;
            vista.panelCentro.remove(panelCarrito.panel1);
            vista.panelCentro.revalidate();
            vista.panelCentro.repaint();
        }
        if(arg.equals("dlmCarr")){
            vista.lblInfo.setText("Ultima Actualizacion Carritos "+ LocalDateTime.now());
        }
        if(arg.equals(AC_CARRITO_PRODUCTO_A))
            vista.lblInfo.setText("Alta CarritoProducto");
        if(arg.equals(AC_CARRITO_PRODUCTO_D))
            vista.lblInfo.setText("Borrado CarritoProducto");
        if(arg.equals(AC_CARRITO_PRODUCTO_M))
            vista.lblInfo.setText("Modificado CarritoProducto");
        if(arg.equals(DISPOSE_PANEL_CARRITO_PRODUCTO)){
            PanelCarritoProducro panelCarritoProducro = (PanelCarritoProducro) o;
            vista.panelCentro.remove(panelCarritoProducro.panel1);
            vista.panelCentro.revalidate();
            vista.panelCentro.repaint();
        }
        if(arg.equals(AC_DRIRECCIONES_ENVIO_A))
            vista.lblInfo.setText("Alta Direccion");
        if(arg.equals(AC_DRIRECCIONES_ENVIO_D))
            vista.lblInfo.setText("Borrado Direccion");
        if(arg.equals(AC_DRIRECCIONES_ENVIO_M))
            vista.lblInfo.setText("Modificado Direccion");
        if(arg.equals(DISPOSE_PANEL_DIR)){
            PanelDirecciones panelDirecciones = (PanelDirecciones) o;
            vista.panelCentro.remove(panelDirecciones.panel1);
            vista.panelCentro.revalidate();
            vista.panelCentro.repaint();
        }
        if(arg.equals("dlmDir")){
            vista.lblInfo.setText("Ultima Actualizacion Direcciones "+ LocalDateTime.now());
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {

        initUsuarios();


    }

    @Override
    public void windowClosing(WindowEvent e) {

        HibernateUtil.closeSessionFactory();
        System.out.println("closeSession");
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
    private void guardarFicheroConfiguracion(){
        Properties configuracion = new Properties();
        configuracion.setProperty(RUTA, ruta.getAbsolutePath());
        configuracion.setProperty(USUARIO, usuario);
        configuracion.setProperty(PASSWORD,password);
        try {
            configuracion.store(new FileOutputStream("fichero.conf"),"fichero de configuracion");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void leerFicheroConfiguracion(){
        Properties configuracion = new Properties();
        try {
            configuracion.load(new FileInputStream("fichero.conf"));
            ruta = new File(configuracion.getProperty(RUTA));
            usuario = configuracion.getProperty(USUARIO);
            password = configuracion.getProperty(PASSWORD);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void initUsuarios(){
        Inicio inicio = new Inicio();
        if(ruta.exists()){
            leerFicheroConfiguracion();
            if(inicio.getEstado() == Inicio.ACEPTADO){
                if ((usuario.equals(inicio.getLogging()) && (password.equals(inicio.getPass())))){
                    startCharge();
                }
                if(usuario.equals(inicio.getLogging()) == false || password.equals(inicio.getPass()) == false){
                    Util.mensajeError("El usuario o la contraseña no coinciden","Error de loggin");
                    System.exit(0);
                }
            }
            if(inicio.getEstado() == Inicio.CANCELADO){
                System.exit(0);
            }

        }
        if(!ruta.exists()){
            if(inicio.getEstado()==Inicio.ACEPTADO){
                this.usuario = inicio.getLogging();
                this.password = inicio.getPass();
                ruta = new File("fichero.conf");
                guardarFicheroConfiguracion();
                startCharge();
            }
            if (inicio.getEstado() == Inicio.CANCELADO){
                System.exit(0);
            }
        }
    }
    private void startCharge(){
        SplashScreen s = new SplashScreen();
        Thread hilo = new Thread(new Runnable() {
            @Override
            public void run() {
                HibernateUtil.buildSessionFactory();
                System.out.println("opensessio");
                INICIADO = true;
            }
        });
        s.start();
        hilo.start();
    }

}
