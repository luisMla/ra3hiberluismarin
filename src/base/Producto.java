package base;

import javax.persistence.*;
import java.util.List;

@Entity
public class Producto {
    private int id;
    private String nombre;
    private Double precio;
    private List<CarritoProducto> carritoProductosById;
    private Operario operarioByIdOperario;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "precio")
    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Producto producto = (Producto) o;

        if (id != producto.id) return false;
        if (nombre != null ? !nombre.equals(producto.nombre) : producto.nombre != null) return false;
        if (precio != null ? !precio.equals(producto.precio) : producto.precio != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        result = 31 * result + (precio != null ? precio.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "productoByIdProducto")
    public List<CarritoProducto> getCarritoProductosById() {
        return carritoProductosById;
    }

    public void setCarritoProductosById(List<CarritoProducto> carritoProductosById) {
        this.carritoProductosById = carritoProductosById;
    }

    @ManyToOne
    @JoinColumn(name = "id_operario", referencedColumnName = "id")
    public Operario getOperarioByIdOperario() {
        return operarioByIdOperario;
    }

    public void setOperarioByIdOperario(Operario operarioByIdOperario) {
        this.operarioByIdOperario = operarioByIdOperario;
    }

    @Override
    public String toString() {
        return "Producto{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", precio=" + precio +
                ", operarioByIdOperario=" + operarioByIdOperario +
                '}';
    }
}
