package giu;

import base.Operario;
import base.Producto;
import tareas.TareaOperario;
import tareas.TareaProducto;
import util.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import static util.Constantes.*;

public class PanelProducto extends Observable implements ActionListener,ListSelectionListener, PropertyChangeListener {
    public JPanel panel1;
    private JTextField tfNombreProducto;
    private JLabel lblNombre;
    private JTextField tfPrecio;
    private JButton btAlta;
    private JList listOp;
    private JButton btBorrar;
    private JButton btModificar;
    private JPanel panelRepintar;
    private JButton btDispose;
    private JLabel lblPrecio;
    private JList listProd;

    private DefaultListModel dlmOperario;
    private DefaultListModel<Producto>dlmProducto;
    private Operario operario;
    private Producto producto;
    private ArrayList<Operario> operarios;
    private ArrayList<Producto> productos;
    Observer observer;
    private TareaOperario tareaOperario;
    private TareaProducto tareaProducto;

    public PanelProducto(Observer observer) {
        this.observer = observer;
        btAlta.setActionCommand(AC_PRODUCTO_A);
        btModificar.setActionCommand(AC_PRODUCTO_M);
        btBorrar.setActionCommand(AC_PRODUCTO_D);
        btDispose.setActionCommand(AC_DISPOSE);
        operarios = new ArrayList<>();
        productos = new ArrayList<>();
        dlmOperario = new DefaultListModel();
        listOp.setModel(dlmOperario);
        dlmProducto = new DefaultListModel<>();
        listProd.setModel(dlmOperario);
        tareaOperario = new TareaOperario(operarios,dlmOperario,"");
        tareaProducto = new TareaProducto(productos,dlmProducto,"");

        initActionListener(this);
        initListSelectionListener(this);
        initPropertyChangeListener(this);
        tareaOperario.execute();
        //tareaProducto.execute();


    }
    private void initActionListener(ActionListener actionListener) {
        btAlta.addActionListener(actionListener);
        btModificar.addActionListener(actionListener);
        btBorrar.addActionListener(actionListener);
        btDispose.addActionListener(actionListener);
    }

    private void initListSelectionListener(ListSelectionListener listSelectionListener) {
        listOp.addListSelectionListener(listSelectionListener);
        listProd.addListSelectionListener(listSelectionListener);
    }
    private void initPropertyChangeListener(PropertyChangeListener propertyChangeListener){
        tareaOperario.addPropertyChangeListener(propertyChangeListener);
        tareaProducto.addPropertyChangeListener(propertyChangeListener);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        tareaOperario.cancel(false);
        tareaProducto.cancel(false);
        switch (e.getActionCommand()){
            case AC_PRODUCTO_A:
                producto = new Producto();
                producto.setNombre(tfNombreProducto.getText());
                producto.setPrecio(Double.parseDouble(tfPrecio.getText()));
                producto.setOperarioByIdOperario(operario);
                tareaProducto = new TareaProducto(producto,productos,dlmProducto,ALTA);
                initPropertyChangeListener(this);
                tareaProducto.execute();
                observer.update(this, AC_PRODUCTO_A);
                break;
            case AC_PRODUCTO_M:
                producto.setNombre(tfNombreProducto.getText());
                producto.setPrecio(Double.parseDouble(tfPrecio.getText()));
                producto.setOperarioByIdOperario(operario);
                tareaProducto = new TareaProducto(producto,productos,dlmProducto,MODI);
                initPropertyChangeListener(this);
                tareaProducto.execute();
                observer.update(this, AC_PRODUCTO_M);
                break;
            case AC_PRODUCTO_D:
                tareaProducto = new TareaProducto(producto,productos,dlmProducto,BAJA);
                initPropertyChangeListener(this);
                tareaProducto.execute();
                observer.update(this, AC_PRODUCTO_D);
                break;
            case AC_DISPOSE:
                tareaProducto.cancel(false);
                tareaOperario.cancel(false);
                observer.update(this, DISPOSE_PANEL_PRODUCTO);
                break;

        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if(evt.getPropertyName().equals("dlmPr")){
            dlmProducto = (DefaultListModel<Producto>) evt.getNewValue();
            listProd.setModel(dlmProducto);
            observer.update(this,"dlmPr");
        }
        if(evt.getPropertyName().equals("dlmO")){
            dlmOperario = (DefaultListModel) evt.getNewValue();
            listOp.setModel(dlmOperario);
            tareaProducto.execute();
        }
        if(evt.getPropertyName().equals("done")){
            if(evt.getNewValue().equals(1)){
                tareaProducto.execute();
            }
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()==true) {
            if (e.getSource().equals(listOp)) {
                operario = (Operario) listOp.getSelectedValue();

            }
            if (e.getSource().equals(listProd)) {
                producto = (Producto) listProd.getSelectedValue();
                tfNombreProducto.setText(producto.getNombre());
                tfPrecio.setText(String.valueOf(producto.getPrecio()));
                operario = producto.getOperarioByIdOperario();
                listOp.setSelectedValue(operario,true);
            }
        }
    }
}
