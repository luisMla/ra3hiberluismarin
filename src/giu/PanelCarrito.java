package giu;

import base.Carrito;
import base.Cliente;
import base.Operario;
import base.Producto;
import tareas.TareaCarrito;
import tareas.TareaCliente;
import tareas.TareaProducto;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import static util.Constantes.*;

public class PanelCarrito extends Observable implements ActionListener,ListSelectionListener, PropertyChangeListener {
    public JPanel panel1;
    private JButton btAlta;
    private JList listCarr;
    private JButton btBorrar;
    private JButton btModificar;
    private JPanel panelRepintar;
    private JButton btDispose;
    private JList listCli;

    private Observer observer;
    private Carrito carrito;
    private Cliente cliente;
    private DefaultListModel<Cliente>dlmCliente;
    private DefaultListModel<Carrito>dlmCarrito;
    private ArrayList<Cliente>clientes;
    private ArrayList<Carrito>carritos;
    TareaCliente tareaCliente;
    TareaCarrito tareaCarrito;

    public PanelCarrito(Observer observer) {
        this.observer = observer;
        btAlta.setActionCommand(AC_CARRITO_A);
        btModificar.setActionCommand(AC_CARRITO_M);
        btBorrar.setActionCommand(AC_CARRITO_D);
        btDispose.setActionCommand(AC_DISPOSE);
        dlmCarrito = new DefaultListModel<>();
        dlmCliente = new DefaultListModel<>();
        clientes = new ArrayList<>();
        carritos = new ArrayList<>();

        tareaCliente = new TareaCliente(clientes,dlmCliente,"");
        tareaCarrito = new TareaCarrito(carritos,dlmCarrito,"");
        tareaCliente.execute();
        initActionListener(this);
        initListSelectionListener(this);
        initPropertyChangeListener(this);


    }

    private void initActionListener(ActionListener actionListener) {
        btAlta.addActionListener(actionListener);
        btModificar.addActionListener(actionListener);
        btBorrar.addActionListener(actionListener);
        btDispose.addActionListener(actionListener);
    }

    private void initListSelectionListener(ListSelectionListener listSelectionListener) {
        listCarr.addListSelectionListener(listSelectionListener);
        listCli.addListSelectionListener(listSelectionListener);
    }
    private void initPropertyChangeListener(PropertyChangeListener propertyChangeListener){
        tareaCliente.addPropertyChangeListener(propertyChangeListener);
        tareaCarrito.addPropertyChangeListener(propertyChangeListener);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        tareaCarrito.cancel(false);
        tareaCliente.cancel(false);
        switch (e.getActionCommand()){
            case AC_CARRITO_A:
                carrito = new Carrito();
                carrito.setClienteByIdCliente(cliente);
                carrito.setTotal(0.0);
                tareaCarrito = new TareaCarrito(carrito,carritos,dlmCarrito,ALTA);
                initPropertyChangeListener(this);
                tareaCarrito.execute();
                observer.update(this, AC_CARRITO_A);
                break;
            case AC_CARRITO_M:
                carrito.setClienteByIdCliente(cliente);
                tareaCarrito = new TareaCarrito(carrito,carritos,dlmCarrito,MODI);
                initPropertyChangeListener(this);
                tareaCarrito.execute();
                observer.update(this, AC_CARRITO_M);
                break;
            case AC_CARRITO_D:
                tareaCarrito = new TareaCarrito(carrito,carritos,dlmCarrito,BAJA);
                initPropertyChangeListener(this);
                tareaCarrito.execute();
                observer.update(this, AC_CARRITO_D);
                break;
            case AC_DISPOSE:
                tareaCarrito.cancel(false);
                tareaCliente.cancel(false);
                observer.update(this, DISPOSE_PANEL_CARRITO);
                break;

        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if(evt.getPropertyName().equals("dlmCarr")){
            dlmCarrito = (DefaultListModel<Carrito>) evt.getNewValue();
            listCarr.setModel(dlmCarrito);
            observer.update(this,"dlmCarr");
        }
        if(evt.getPropertyName().equals("dlm")){
            dlmCliente = (DefaultListModel<Cliente>) evt.getNewValue();
            listCli.setModel(dlmCliente);
            tareaCarrito.execute();
        }
        if(evt.getPropertyName().equals("done")){
            if(evt.getNewValue().equals(1)){
                tareaCarrito.execute();
            }
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()==true) {
            if (e.getSource().equals(listCli)) {
                cliente = (Cliente) listCli.getSelectedValue();

            }
            if (e.getSource().equals(listCarr)) {
                carrito = (Carrito) listCarr.getSelectedValue();
                cliente = carrito.getClienteByIdCliente();
                listCli.setSelectedValue(cliente,true);
            }
        }
    }
}
