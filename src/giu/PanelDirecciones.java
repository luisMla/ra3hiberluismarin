package giu;

import base.Cliente;
import base.DireccionesEnvio;
import tareas.TareaCliente;
import tareas.TareaDir;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import static util.Constantes.*;

public class PanelDirecciones extends Observable implements ActionListener,ListSelectionListener, PropertyChangeListener {
    public JPanel panel1;
    private JTextField tfDireccion;
    private JLabel lblNombre;
    private JTextField tfCP;
    private JButton btAlta;
    private JList listDir;
    private JButton btBorrar;
    private JButton btModificar;
    private JPanel panelRepintar;
    private JButton btDispose;
    private JLabel lblPrecio;
    private JList listCli;

    private DefaultListModel<Cliente>dlmCliente;
    private ArrayList<Cliente>clientes;
    private Cliente cliente;

    private DefaultListModel<DireccionesEnvio>dlmDireccion;
    private ArrayList<DireccionesEnvio>direcciones;
    private DireccionesEnvio direccionesEnvio;

    private Observer observer;

    private TareaCliente tareaCliente;
    private TareaDir tareaDir;


    public PanelDirecciones(Observer observer) {
        this.observer = observer;
        btAlta.setActionCommand(AC_DRIRECCIONES_ENVIO_A);
        btBorrar.setActionCommand(AC_DRIRECCIONES_ENVIO_D);
        btModificar.setActionCommand(AC_DRIRECCIONES_ENVIO_M);
        btDispose.setActionCommand(AC_DISPOSE);

        direcciones = new ArrayList<>();
        clientes = new ArrayList<>();
        dlmCliente = new DefaultListModel<>();
        dlmDireccion = new DefaultListModel<>();
        tareaCliente = new TareaCliente(clientes,dlmCliente, "");
        tareaDir = new TareaDir(direcciones,dlmDireccion,"");
        initActionListener(this);
        initFirePropertyChengeListener(this);
        initListSelectionListener(this);

        tareaCliente.execute();
    }
    private void initActionListener(ActionListener actionListener){
        btAlta.addActionListener(actionListener);
        btBorrar.addActionListener(actionListener);
        btModificar.addActionListener(actionListener);
        btDispose.addActionListener(actionListener);
    }
    private void  initListSelectionListener(ListSelectionListener listSelectionListener){
        listCli.addListSelectionListener(listSelectionListener);
        listDir.addListSelectionListener(listSelectionListener);
    }
    private void initFirePropertyChengeListener(PropertyChangeListener propertyChangeListener){
        tareaCliente.addPropertyChangeListener(propertyChangeListener);
        tareaDir.addPropertyChangeListener(propertyChangeListener);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        tareaCliente.cancel(false);
        tareaDir.cancel(false);
        tareaCliente = new TareaCliente(clientes,dlmCliente,"");
        switch (e.getActionCommand()){
            case AC_DRIRECCIONES_ENVIO_A:
                direccionesEnvio = new DireccionesEnvio();
                direccionesEnvio.setCalle(tfDireccion.getText());
                direccionesEnvio.setCodigoPosta(tfCP.getText());
                direccionesEnvio.setClienteByIdCliente(cliente);
                tareaDir = new TareaDir(direccionesEnvio,direcciones,dlmDireccion,ALTA);
                initFirePropertyChengeListener(this);
                tareaCliente.execute();
                observer.update(this,AC_DRIRECCIONES_ENVIO_A);
                break;
            case AC_DRIRECCIONES_ENVIO_M:
                direccionesEnvio.setCalle(tfDireccion.getText());
                direccionesEnvio.setCodigoPosta(tfCP.getText());
                direccionesEnvio.setClienteByIdCliente(cliente);
                tareaDir = new TareaDir(direccionesEnvio,direcciones,dlmDireccion,MODI);
                initFirePropertyChengeListener(this);
                tareaDir.execute();
                observer.update(this,AC_DRIRECCIONES_ENVIO_M);
                break;
            case AC_DRIRECCIONES_ENVIO_D:
                tareaDir = new TareaDir(direccionesEnvio,direcciones,dlmDireccion,BAJA);
                initFirePropertyChengeListener(this);
                tareaDir.execute();
                observer.update(this,AC_DRIRECCIONES_ENVIO_D);
                break;
            case AC_DISPOSE:
                tareaCliente.cancel(false);
                tareaDir.cancel(false);
                observer.update(this, DISPOSE_PANEL_DIR);
                break;
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if(evt.getPropertyName().equals("dlm")){
            System.out.println("dlm");
            dlmCliente = (DefaultListModel<Cliente>) evt.getNewValue();
            listCli.setModel(dlmCliente);
            observer.update(this,"dlm");
            if(tareaDir.getState().toString().equals(PENDING)){
                tareaDir.execute();
            }
        }
        if(evt.getPropertyName().equals("dlmDir")){
            dlmDireccion = (DefaultListModel<DireccionesEnvio>) evt.getNewValue();
            listDir.setModel(dlmDireccion);
            observer.update(this, "dlmDir");
            if(tareaCliente.getState().toString().equals(PENDING)){
                tareaCliente.execute();
            }
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()==true){
            if(e.getSource().equals(listCli)){
                cliente = (Cliente) listCli.getSelectedValue();
            }
            if(e.getSource().equals(listDir)){
                direccionesEnvio = (DireccionesEnvio) listDir.getSelectedValue();
                tfDireccion.setText(direccionesEnvio.getCalle());
                tfCP.setText(direccionesEnvio.getCodigoPosta());
                listCli.setSelectedValue(direccionesEnvio.getClienteByIdCliente(),true);
            }
        }
    }
}
