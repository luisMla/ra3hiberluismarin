package tareas;

import base.Cliente;
import base.HibernateUtil;
import org.hibernate.Session;

import javax.management.Query;
import javax.swing.*;
import java.util.ArrayList;

import static util.Constantes.ALTA;
import static util.Constantes.BAJA;
import static util.Constantes.MODI;

public class TareaCliente extends SwingWorker<Void, Integer>{
    private Cliente cliente;
    private ArrayList<Cliente> clientes;
    private DefaultListModel<Cliente> dlmCliente;
    private String condicion;

    public TareaCliente(Cliente cliente, ArrayList<Cliente> clientes,DefaultListModel<Cliente>dlmCliente, String condicion) {
        this.cliente = cliente;
        this.clientes = clientes;
        this.condicion = condicion;
        this.dlmCliente = dlmCliente;
    }
    public TareaCliente( ArrayList<Cliente> clientes, DefaultListModel<Cliente>dlmCliente,String condicion) {
        this.cliente = null;
        this.clientes = clientes;
        this.condicion = condicion;
        this.dlmCliente = dlmCliente;
    }

    @Override
    protected Void doInBackground() throws Exception {
        Session session = HibernateUtil.getCurrentSession();
        switch (condicion){
            case ALTA:
                session.beginTransaction();
                session.save(cliente);
                session.getTransaction().commit();
                break;
            case BAJA:
                session.beginTransaction();
                session.delete(cliente);
                session.getTransaction().commit();
                break;

            case  MODI:
                session.beginTransaction();
                session.saveOrUpdate(cliente);
                session.getTransaction().commit();
                break;
        }
        while (!isCancelled()) {
            Session sess = HibernateUtil.getCurrentSession();
            org.hibernate.query.Query query = sess.createQuery("FROM base.Cliente");
            clientes = (ArrayList<Cliente>) query.getResultList();
            //dlmCliente.clear();
            dlmCliente = new DefaultListModel<>();
            for (Cliente cli : clientes) {
                dlmCliente.addElement(cli);
                System.out.println(cli);
            }

            firePropertyChange("dlm", 0, dlmCliente);
            firePropertyChange("cliente",0, clientes);
            System.out.println("refrescar");
            Thread.sleep(10000);
            sess.close();
        }
       // session.close();
        return null;
    }

    @Override
    protected void done() {
        firePropertyChange("done",0,1);
    }
}
