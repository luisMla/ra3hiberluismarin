package giu;

import javax.swing.*;
import java.awt.event.*;

/**
 * @author luis marin lafuente
 * ventana de loggin de la aplicacion principal
 *
 */
public class Inicio extends JDialog {
    private JPanel contentPanel;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField tfUsuario;
    private JPasswordField passwordField1;
    private JLabel tfPassword;

    private String logging;
    private String pass;
    private int estado;

    public final static int ACEPTADO = 0;
    public final static int CANCELADO = 1;

    /**
     * constructor por defecto
     */
    public Inicio() {
        setContentPane(contentPanel);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setLocationRelativeTo(null);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPanel.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        pack();
        setVisible(true);
    }

    private void onOK() {
        // add your code here
        logging  = tfUsuario.getText();
        pass = String.copyValueOf(passwordField1.getPassword());
        this.estado = ACEPTADO;
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        this.estado = CANCELADO;
        dispose();
    }

    /**
     * devuelve el nombre de usuario metido como loggin
     * @return String
     */
    public String getLogging() {
        return logging;
    }

    /**
     * devuelve el passwors introducido en un string
     * @return String
     */
    public String getPass() {
        return pass;
    }

    /**
     * devuelve el estado de respuesta de los botones de la ventana
     * @return int
     */
    public int getEstado() {
        return estado;
    }

}
