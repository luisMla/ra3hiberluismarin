package tareas;

import base.Carrito;
import base.CarritoProducto;
import base.HibernateUtil;
import base.Operario;
import org.hibernate.Session;
import org.hibernate.query.Query;


import javax.swing.*;
import java.util.ArrayList;

import static util.Constantes.ALTA;
import static util.Constantes.BAJA;
import static util.Constantes.MODI;

public class TareaCarritoProducto extends SwingWorker<Void,Integer> {
    private CarritoProducto carritoProducto;
    private ArrayList<CarritoProducto>carritoProductos;
    private DefaultListModel<CarritoProducto>dlmCarritoProducto;
    private String condicion;

    public TareaCarritoProducto(ArrayList<CarritoProducto> carritoProductos, DefaultListModel<CarritoProducto> dlmCarritoProducto, String condicion) {
        this.carritoProductos = carritoProductos;
        this.dlmCarritoProducto = dlmCarritoProducto;
        this.condicion = condicion;
    }

    public TareaCarritoProducto(CarritoProducto carritoProducto, ArrayList<CarritoProducto> carritoProductos, DefaultListModel<CarritoProducto> dlmCarritoProducto, String condicion) {
        this.carritoProducto = carritoProducto;
        this.carritoProductos = carritoProductos;
        this.dlmCarritoProducto = dlmCarritoProducto;
        this.condicion = condicion;
    }

    @Override
    protected Void doInBackground() throws Exception {
        Session session = HibernateUtil.getCurrentSession();
        switch (condicion){
            case ALTA:
                session.beginTransaction();
                session.save(carritoProducto);
                session.getTransaction().commit();
                firePropertyChange("crud",0,0);
                break;
            case BAJA:
                session.beginTransaction();
                session.delete(carritoProducto);
                session.getTransaction().commit();
                firePropertyChange("crud",0,0);

                break;
            case MODI:
                session.beginTransaction();
                session.saveOrUpdate(carritoProducto);
                session.getTransaction().commit();
                firePropertyChange("crud",0,0);
                break;

        }
        session.close();
        while (!isCancelled()&&condicion.equals("")) {
            Session sess = HibernateUtil.getCurrentSession();
            org.hibernate.query.Query query = sess.createQuery("FROM base.CarritoProducto");
            carritoProductos = (ArrayList<CarritoProducto>) query.getResultList();
            dlmCarritoProducto = new DefaultListModel<>();
            for (CarritoProducto cp : carritoProductos) {
                dlmCarritoProducto.addElement(cp);
                System.out.println(cp);
            }

            firePropertyChange("dlmCp", 0, dlmCarritoProducto);
            firePropertyChange("carrot",0, carritoProductos);
            System.out.println("refrescar");
            Thread.sleep(10000);
            sess.close();
        }
        return null;
    }

    @Override
    protected void done() {
        firePropertyChange("done1",0,0);
        super.done();
    }
}
