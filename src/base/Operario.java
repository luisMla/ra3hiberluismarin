package base;

import javax.persistence.*;
import java.util.List;

@Entity
public class Operario {
    private int id;
    private String nombre;
    private Integer edad;
    private List<Producto> productosById;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "edad")
    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Operario operario = (Operario) o;

        if (id != operario.id) return false;
        if (nombre != null ? !nombre.equals(operario.nombre) : operario.nombre != null) return false;
        if (edad != null ? !edad.equals(operario.edad) : operario.edad != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        result = 31 * result + (edad != null ? edad.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "operarioByIdOperario")
    public List<Producto> getProductosById() {
        return productosById;
    }

    public void setProductosById(List<Producto> productosById) {
        this.productosById = productosById;
    }

    @Override
    public String toString() {
        return "Operario{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", edad=" + edad +
                '}';
    }
}
