package tareas;

import base.DireccionesEnvio;
import base.HibernateUtil;
import base.Operario;
import org.hibernate.Session;

import javax.swing.*;
import java.util.ArrayList;

import static util.Constantes.ALTA;
import static util.Constantes.BAJA;
import static util.Constantes.MODI;

public class TareaDir extends SwingWorker<Void,Integer> {
    private DireccionesEnvio direccionesEnvio;
    private ArrayList<DireccionesEnvio>direcciones;
    private DefaultListModel<DireccionesEnvio>dlmDirecciones;
    private String condicion;

    public TareaDir(DireccionesEnvio direccionesEnvio, ArrayList<DireccionesEnvio> direcciones, DefaultListModel<DireccionesEnvio> dlmDirecciones, String condicion) {
        this.direccionesEnvio = direccionesEnvio;
        this.direcciones = direcciones;
        this.dlmDirecciones = dlmDirecciones;
        this.condicion = condicion;
    }

    public TareaDir(ArrayList<DireccionesEnvio> direcciones, DefaultListModel<DireccionesEnvio> dlmDirecciones, String condicion) {
        this.direcciones = direcciones;
        this.dlmDirecciones = dlmDirecciones;
        this.condicion = condicion;
        this.direccionesEnvio = null;
    }

    @Override
    protected Void doInBackground() throws Exception {
        Session session = HibernateUtil.getCurrentSession();
        switch (condicion){
            case ALTA:
                session.beginTransaction();
                session.save(direccionesEnvio);
                session.getTransaction().commit();
                break;
            case BAJA:
                session.beginTransaction();
                session.delete(direccionesEnvio);
                session.getTransaction().commit();
                break;

            case  MODI:
                session.beginTransaction();
                session.saveOrUpdate(direccionesEnvio);
                session.getTransaction().commit();
                break;
        }
        while (!isCancelled()) {
            Session sess = HibernateUtil.getCurrentSession();
            org.hibernate.query.Query query = sess.createQuery("FROM base.DireccionesEnvio");
            direcciones = (ArrayList<DireccionesEnvio>) query.getResultList();
            dlmDirecciones = new DefaultListModel<>();
            for (DireccionesEnvio dir : direcciones) {
                dlmDirecciones.addElement(dir);
                System.out.println(dir);
            }

            firePropertyChange("dlmDir", 0, dlmDirecciones);
            firePropertyChange("Ditecciones",0, direcciones);
            System.out.println("refrescar");
            Thread.sleep(10000);
            sess.close();
        }
        return null;
    }

    @Override
    protected void done() {
        firePropertyChange("done",0,0);
    }
}
