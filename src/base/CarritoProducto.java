package base;

import javax.persistence.*;

@Entity
@Table(name = "carrito_producto", schema = "hibernate", catalog = "")
public class CarritoProducto {
    private int id;
    private Integer cantidad;
    private Producto productoByIdProducto;
    private Carrito carritoByIdCarrito;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "cantidad")
    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CarritoProducto that = (CarritoProducto) o;

        if (id != that.id) return false;
        if (cantidad != null ? !cantidad.equals(that.cantidad) : that.cantidad != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (cantidad != null ? cantidad.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "id_producto", referencedColumnName = "id")
    public Producto getProductoByIdProducto() {
        return productoByIdProducto;
    }

    public void setProductoByIdProducto(Producto productoByIdProducto) {
        this.productoByIdProducto = productoByIdProducto;
    }

    @ManyToOne
    @JoinColumn(name = "id_carrito", referencedColumnName = "id")
    public Carrito getCarritoByIdCarrito() {
        return carritoByIdCarrito;
    }

    public void setCarritoByIdCarrito(Carrito carritoByIdCarrito) {
        this.carritoByIdCarrito = carritoByIdCarrito;
    }

    @Override
    public String toString() {
        return "CarritoProducto{" +
                "id=" + id +
                ", cantidad=" + cantidad +
                ", productoByIdProducto=" + productoByIdProducto.getNombre() +
                '}';
    }
}
