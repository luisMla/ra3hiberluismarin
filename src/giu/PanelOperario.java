package giu;

import base.Cliente;
import base.Operario;
import tareas.TareaCliente;
import tareas.TareaOperario;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import static util.Constantes.*;

public class PanelOperario extends Observable implements ActionListener,ListSelectionListener, PropertyChangeListener {
    public JPanel panel1;
    private JTextField tfNombreOperario;
    private JLabel lblNombre;
    private JTextField tfEdad;
    private JButton btAlta;
    private JList listOp;
    private JButton btBorrar;
    private JButton btModificar;
    private JPanel panelRepintar;
    private JButton btDispose;
    private JLabel lblEdad;

    private DefaultListModel dlmOperario;
    private Operario operario;
    private ArrayList<Operario>operarios;
    Observer observer;
    private TareaOperario tareaOperario;

    public PanelOperario(Observer observer) {
        this.observer = observer;
        btAlta.setActionCommand(AC_OPERARIO_A);
        btModificar.setActionCommand(AC_OPERARIO_M);
        btBorrar.setActionCommand(AC_OPERARIO_D);
        btDispose.setActionCommand(AC_DISPOSE);

        dlmOperario = new DefaultListModel<>();
        operarios = new ArrayList<>();
        listOp.setModel(dlmOperario);
        operarios = new ArrayList<>();
        tareaOperario = new TareaOperario( operarios,dlmOperario,"");
        initActionListener(this);
        initListSelectionListener(this);
        initPropertyChangeListener(this);
        tareaOperario.execute();
    }
    private void initActionListener(ActionListener actionListener) {
        btAlta.addActionListener(actionListener);
        btModificar.addActionListener(actionListener);
        btBorrar.addActionListener(actionListener);
        btDispose.addActionListener(actionListener);
    }

    private void initListSelectionListener(ListSelectionListener listSelectionListener) {
        listOp.addListSelectionListener(listSelectionListener);
    }
    private void initPropertyChangeListener(PropertyChangeListener propertyChangeListener){
        tareaOperario.addPropertyChangeListener(propertyChangeListener);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        tareaOperario.cancel(false);
        switch (e.getActionCommand()) {
            case AC_OPERARIO_A:
                operario = new Operario();
                operario.setNombre(tfNombreOperario.getText());
                operario.setEdad((Integer) Integer.parseInt(tfEdad.getText()));
                observer.update(this, AC_OPERARIO_A);
                tareaOperario = new TareaOperario(operario,operarios,dlmOperario,ALTA);
                initPropertyChangeListener(this);
                tareaOperario.execute();

                break;
            case AC_OPERARIO_M:
                operario.setNombre(tfNombreOperario.getText());
                operario.setEdad((Integer) Integer.parseInt(tfEdad.getText()));
                observer.update(this, AC_OPERARIO_M);
                tareaOperario = new TareaOperario(operario,operarios,dlmOperario,MODI);
                initPropertyChangeListener(this);
                tareaOperario.execute();
                break;
            case AC_OPERARIO_D:
                observer.update(this, AC_OPERARIO_D);
                tareaOperario = new TareaOperario(operario,operarios,dlmOperario,BAJA);
                initPropertyChangeListener(this);
                tareaOperario.execute();
                break;
            case AC_DISPOSE:
                observer.update(this,DISPOSE_PANEL_OPERARIO);
                tareaOperario.cancel(false);

        }
        System.out.println("fuera swich action comand");
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equals(ALTA)) {
            System.out.println("alta");
        }
        if (evt.getPropertyName().equals(BAJA)) {
            System.out.println("baja");
        }
        if (evt.getPropertyName().equals(MODI)) {
            System.out.println("modi");
        }
        if (evt.getPropertyName().equals("done")){
            System.out.println("done");
            // pintarClientes();

        }
        if(evt.getPropertyName().equals("dlmO")){
            dlmOperario = (DefaultListModel<Operario>) evt.getNewValue();
            listOp.setModel(dlmOperario);
            observer.update(this,"dlmO");
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()==true)
            if (e.getSource().equals(listOp)) {
                operario = (Operario) listOp.getSelectedValue();
                tfNombreOperario.setText(operario.getNombre());
                tfEdad.setText(String.valueOf(operario.getEdad()));
            }
    }
}
