package util;

public class Constantes {
    public static final String ARCHIVO = "Archivo";
    public static final String CARRITO = "Carrito";
    public static final String CARRITO_PRODUCTO = "Carrito Producto";
    public static final String CLIENTE = "Cliente";
    public static final String DRIRECCIONES_ENVIO = "Direcciones envio";
    public static final String OPERARIO = "Operario";
    public static final String PRODUCTO = "Producto";
    public static final String OPERACIONES = "Operaciones ";

    public static final String AC_CARRITO_A = "aCarrito";
    public static final String AC_CARRITO_PRODUCTO_A = "aCarrito Producto";
    public static final String AC_CLIENTE_A = "aCliente";
    public static final String AC_DRIRECCIONES_ENVIO_A = "aDirecciones envio";
    public static final String AC_OPERARIO_A = "aOperario";
    public static final String AC_PRODUCTO_A = "aProducto";

    public static final String AC_CARRITO_D = "dCarrito";
    public static final String AC_CARRITO_PRODUCTO_D = "dCarrito Producto";
    public static final String AC_CLIENTE_D = "dCliente";
    public static final String AC_DRIRECCIONES_ENVIO_D = "dDirecciones envio";
    public static final String AC_OPERARIO_D = "dOperario";
    public static final String AC_PRODUCTO_D = "dProducto";

    public static final String AC_CARRITO_M = "mCarrito";
    public static final String AC_CARRITO_PRODUCTO_M = "mCarrito Producto";
    public static final String AC_CLIENTE_M = "mCliente";
    public static final String AC_DRIRECCIONES_ENVIO_M = "mDirecciones envio";
    public static final String AC_OPERARIO_M = "mOperario";
    public static final String AC_PRODUCTO_M = "mProducto";

    public static final String AC_DISPOSE = "dispose";
    public static final String AC_BUSCAR = "AC_BUSCAR";
    public static final String AC_LIMPIAR = "AC_LIMPIAR";
    public static final String SALIR = "Salir";
    public static final String ALTA = "Alta";
    public static final String BAJA = "Baja";
    public static final String MODI = "Modificar";

    public static final String DISPOSE_PANEL_CLIENTE = "PANCLIDIS";
    public static final String DISPOSE_PANEL_OPERARIO = "PANOPDIS";
    public static final String DISPOSE_PANEL_PRODUCTO = "PANPRODIS";
    public static final String DISPOSE_PANEL_CARRITO = "PANCARRDIS";
    public static final String DISPOSE_PANEL_CARRITO_PRODUCTO = "DISPOSE_PANEL_CARRITO_PRODUCTO";
    public static final String DISPOSE_PANEL_DIR = "DISPOSE_PANEL_DIR";
    public static final String PENDING = "PENDING";
    public static final String STARTED = "STARTED";
    public static final String DONE = "DONE";
    public static  String RUTA ="";
    public static final String USUARIO = "USUARIO";
    public static final String PASSWORD = "PASSWORD";

    public static boolean INICIADO = false;
}
