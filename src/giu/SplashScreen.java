package giu;

import base.HibernateUtil;

import javax.swing.*;

import java.net.URL;

import static util.Constantes.INICIADO;

public class SplashScreen extends Thread{
    JFrame frame;
    private JPanel panel1;
    private JLabel lblImg;

    public SplashScreen() {
        frame = new JFrame("SplashScreen");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setUndecorated(true);
        frame.pack();
        frame.setLocationRelativeTo(null);
    }
    @Override
    public void run() {
        frame.setVisible(true);
        while (!INICIADO){
            System.out.println("HibernateCargando");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        frame.dispose();
    }

}
