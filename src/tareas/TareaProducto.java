package tareas;

import base.Cliente;
import base.HibernateUtil;
import base.Producto;
import org.hibernate.Session;
import org.hibernate.engine.jdbc.spi.SqlExceptionHelper;
import util.Util;

import javax.swing.*;
import java.util.ArrayList;

import static util.Constantes.ALTA;
import static util.Constantes.BAJA;
import static util.Constantes.MODI;

public class TareaProducto extends SwingWorker<Void, Integer> {
    private Producto producto;
    private ArrayList<Producto> productos;
    private DefaultListModel<Producto>dlmProducto;
    private String condicion;

    public TareaProducto(Producto producto, ArrayList<Producto> productos,DefaultListModel<Producto>dlmProducto, String condicion) {
        this.producto = producto;
        this.dlmProducto = dlmProducto;
        this.productos = productos;
        this.condicion = condicion;
    }

    public TareaProducto(ArrayList<Producto> productos, DefaultListModel<Producto> dlmProducto, String condicion) {
        this.productos = productos;
        this.dlmProducto = dlmProducto;
        this.condicion = condicion;
        this.producto = null;
    }

    @Override
    protected Void doInBackground() throws Exception {
        Session session = HibernateUtil.getCurrentSession();
        switch (condicion){
            case ALTA:
                session.beginTransaction();
                session.save(producto);
                session.getTransaction().commit();
                break;
            case MODI:
                session.beginTransaction();
                session.saveOrUpdate(producto);
                session.getTransaction().commit();
                break;
            case BAJA:
                try {
                    session.beginTransaction();
                    session.delete(producto);
                    session.getTransaction().commit();
                }catch (Exception e){
                    Util.mensajeError("No se puede eliminar id en uso","ERROR");
                }
                break;
        }
        while (!isCancelled()) {
            Session sess = HibernateUtil.getCurrentSession();
            org.hibernate.query.Query query = sess.createQuery("FROM base.Producto");
            productos = (ArrayList<Producto>) query.getResultList();
            //dlmCliente.clear();
            dlmProducto = new DefaultListModel<>();
            for (Producto pr : productos) {
                dlmProducto.addElement(pr);
                System.out.println(pr);
            }

            firePropertyChange("dlmPr", 0, dlmProducto);
            firePropertyChange("productos",0, productos);
            System.out.println("refrescar");
            Thread.sleep(10000);
            sess.close();
        }
        return null;
    }

    @Override
    protected void done() {
        firePropertyChange("done",0,2);
    }
}
