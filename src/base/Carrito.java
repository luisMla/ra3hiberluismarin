package base;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Carrito {
    private int id;
    private Timestamp fechaPedido;
    private Double total;
    private Cliente clienteByIdCliente;
    private List<CarritoProducto> carritoProductosById;//todo iniciar si al crear añado carProf

    public Carrito() {
        //carritoProductosById = new ArrayList<>();
    }

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "fecha_pedido")
    public Timestamp getFechaPedido() {
        return fechaPedido;
    }

    public void setFechaPedido(Timestamp fechaPedido) {
        this.fechaPedido = fechaPedido;
    }

    @Basic
    @Column(name = "total")
    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Carrito carrito = (Carrito) o;

        if (id != carrito.id) return false;
        if (fechaPedido != null ? !fechaPedido.equals(carrito.fechaPedido) : carrito.fechaPedido != null) return false;
        if (total != null ? !total.equals(carrito.total) : carrito.total != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (fechaPedido != null ? fechaPedido.hashCode() : 0);
        result = 31 * result + (total != null ? total.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "id_cliente", referencedColumnName = "id")
    public Cliente getClienteByIdCliente() {
        return clienteByIdCliente;
    }

    public void setClienteByIdCliente(Cliente clienteByIdCliente) {
        this.clienteByIdCliente = clienteByIdCliente;
    }
   // fetch=FetchType.EAGER
    @OneToMany(mappedBy = "carritoByIdCarrito", fetch = FetchType.EAGER)
    public List<CarritoProducto> getCarritoProductosById() {
        return carritoProductosById;
    }

    public void setCarritoProductosById(List<CarritoProducto> carritoProductosById) {
        this.carritoProductosById = carritoProductosById;
    }

    @Override
    public String toString() {
        return "Carrito{" +
                "id=" + id +
                ", fechaPedido=" + fechaPedido +
                ", total=" + total +
                ", clienteByIdCliente=" + clienteByIdCliente +
                '}';
    }
}
