package tareas;

import base.Carrito;
import base.Cliente;
import base.HibernateUtil;
import org.hibernate.Session;

import javax.swing.*;
import java.util.ArrayList;

import static util.Constantes.ALTA;
import static util.Constantes.BAJA;
import static util.Constantes.MODI;

public class TareaCarrito extends SwingWorker<Void, Integer> {
    Carrito carrito;
    ArrayList<Carrito>carritos;
    DefaultListModel<Carrito>dlmCarrito;
    String condicion;

    public TareaCarrito(Carrito carrito, ArrayList<Carrito> carritos, DefaultListModel<Carrito> dlmCarrito, String condicion) {
        this.carrito = carrito;
        this.carritos = carritos;
        this.dlmCarrito = dlmCarrito;
        this.condicion = condicion;
    }

    public TareaCarrito(ArrayList<Carrito> carritos, DefaultListModel<Carrito> dlmCarrito, String condicion) {
        this.carritos = carritos;
        this.dlmCarrito = dlmCarrito;
        this.condicion = condicion;
    }

    @Override
    protected Void doInBackground() throws Exception {
        Session session = HibernateUtil.getCurrentSession();
        switch (condicion){
            case ALTA:
                session.beginTransaction();
                session.save(carrito);
                session.getTransaction().commit();
                break;
            case BAJA:
                session.beginTransaction();
                session.delete(carrito);
                session.getTransaction().commit();
                break;

            case  MODI:
                session.beginTransaction();
                session.saveOrUpdate(carrito);
                session.getTransaction().commit();
                break;
        }
        while (!isCancelled()) {
            Session sess = HibernateUtil.getCurrentSession();
            org.hibernate.query.Query query = sess.createQuery("FROM base.Carrito");
            carritos = (ArrayList<Carrito>) query.getResultList();
            //dlmCliente.clear();
            dlmCarrito = new DefaultListModel<>();
            for (Carrito carr : carritos) {
                dlmCarrito.addElement(carr);
                System.out.println(carr);
            }

            firePropertyChange("dlmCarr", 0, dlmCarrito);
            firePropertyChange("carritos",0, carritos);
            System.out.println("refrescar");
            Thread.sleep(10000);
            sess.close();
        }
        // session.close();
        return null;
    }

    @Override
    protected void done() {
        firePropertyChange("done",0,1);
    }
}
