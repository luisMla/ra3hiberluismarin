package base;

import javax.persistence.*;
import java.util.List;

@Entity
public class Cliente {
    private int id;
    private String nombre;
    private String apellido;
    private List<Carrito> carritosById;
    private List<DireccionesEnvio> direccionesEnviosById;

    public Cliente() {
    }

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apellido")
    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cliente cliente = (Cliente) o;

        if (id != cliente.id) return false;
        if (nombre != null ? !nombre.equals(cliente.nombre) : cliente.nombre != null) return false;
        if (apellido != null ? !apellido.equals(cliente.apellido) : cliente.apellido != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        result = 31 * result + (apellido != null ? apellido.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "clienteByIdCliente")
    public List<Carrito> getCarritosById() {
        return carritosById;
    }

    public void setCarritosById(List<Carrito> carritosById) {
        this.carritosById = carritosById;
    }

    @OneToMany(mappedBy = "clienteByIdCliente")
    public List<DireccionesEnvio> getDireccionesEnviosById() {
        return direccionesEnviosById;
    }

    public void setDireccionesEnviosById(List<DireccionesEnvio> direccionesEnviosById) {
        this.direccionesEnviosById = direccionesEnviosById;
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                '}';
    }
}
