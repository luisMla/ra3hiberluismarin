package tareas;

import base.Cliente;
import base.HibernateUtil;
import base.Operario;
import org.hibernate.Session;

import javax.swing.*;
import java.util.ArrayList;

import static util.Constantes.ALTA;
import static util.Constantes.BAJA;
import static util.Constantes.MODI;

public class TareaOperario extends SwingWorker<Void, Integer> {
    private Operario operario;
    private ArrayList<Operario> operarios;
    private DefaultListModel<Operario>dlmOperario;
    private String condicion;

    public TareaOperario(Operario operario, ArrayList<Operario> operarios,DefaultListModel<Operario>dlmOperario ,String condicion) {
        this.operario = operario;
        this.operarios = operarios;
        this.condicion = condicion;
        this.dlmOperario = dlmOperario;
    }

    public TareaOperario(ArrayList<Operario> operarios, DefaultListModel<Operario> dlmOperario, String condicion) {
        this.operarios = operarios;
        this.dlmOperario = dlmOperario;
        this.condicion = condicion;
        this.operario=null;
    }

    @Override
    protected Void doInBackground() throws Exception {
        Session session = HibernateUtil.getCurrentSession();
        switch (condicion){
            case ALTA:
                session.beginTransaction();
                session.save(operario);
                session.getTransaction().commit();
                break;
            case BAJA:
                session.beginTransaction();
                session.delete(operario);
                session.getTransaction().commit();
                break;

            case  MODI:
                session.beginTransaction();
                session.saveOrUpdate(operario);
                session.getTransaction().commit();
                break;
        }
        while (!isCancelled()) {
            Session sess = HibernateUtil.getCurrentSession();
            org.hibernate.query.Query query = sess.createQuery("FROM base.Operario");
            operarios = (ArrayList<Operario>) query.getResultList();
            dlmOperario = new DefaultListModel<>();
            for (Operario op : operarios) {
                dlmOperario.addElement(op);
                System.out.println(op);
            }

            firePropertyChange("dlmO", 0, dlmOperario);
            firePropertyChange("operarios",0, operarios);
            System.out.println("refrescar");
            Thread.sleep(10000);
            sess.close();
        }
        return null;
    }

    @Override
    protected void done() {
        firePropertyChange("done",0,1);
    }
}
